'use strict'

const path = require('path')
const webpack = require('webpack')
const objectAssign = require('object-assign')
const commonConfig = require('./webpack.config.common')

module.exports = objectAssign(commonConfig, {
  devtool: 'source-map',
  context: __dirname,
  entry: [
    'babel-polyfill', // necessary for Array.find, Object.keys in IE9
    './src/index.js'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'react-bundle.js'
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      }
    }),
    new webpack.optimize.OccurrenceOrderPlugin()
  ]
})
