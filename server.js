const express = require('express')
const webpack = require('webpack')
const entry = /.*\.js$/.test(process.argv[2]) ? process.argv[2] : undefined
const config = require('./webpack.config.hot')(entry)

const app = express()
const compiler = webpack(config)
const args = process.argv.slice(2)

if (args.indexOf('dist') === -1) {
  app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath,
    headers: { 'Access-Control-Allow-Origin': '*' }
  }))

  app.use(require('webpack-hot-middleware')(compiler))
}

app.use('/', express.static('.'))
app.use('/dist/', express.static('dist'))
app.use('/data/', express.static('data'))

/* eslint-disable no-console */
app.listen(3000, 'localhost', function (err) {
  if (err) {
    console.log(err)

    return
  }

  console.log('Listening at http://localhost:3000')
})
/* eslint-enable */
