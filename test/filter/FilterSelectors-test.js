import expect from 'expect'

import * as FilterSelectors from '../../src/filter/FilterSelectors'

describe('FilterSelectors', () => {
  let state

  let filterList
  let selectedFilters

  beforeEach(() => {
    filterList = [ 'filter1', 'filter2', 'filter3' ]
    selectedFilters = [ 'filter1', 'filter2' ]

    state = {
      filter: {
        filterList: filterList,
        selectedFilters: selectedFilters
      }
    }
  })

  describe('filterListSelector', () => {
    it('should retrieve the filterList state', () => {
      expect(FilterSelectors.filterListSelector(state)).toEqual(filterList)
    })
  })

  describe('selectedFiltersSelector', () => {
    it('should retrieve the selectedFilters state', () => {
      expect(FilterSelectors.selectedFiltersSelector(state)).toEqual(selectedFilters)
    })
  })
})
