import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import expect from 'expect'
import sinon from 'sinon'

import {
  SET_FILTER_LIST,
  SET_SELECTED_FILTERS
} from '../../src/filter/FilterConstants'

import * as ProductsGalleryApi from '../../src/api/ProductsGalleryApi'

import * as AppActions from '../../src/AppActions'
import * as FilterActions from '../../src/filter/FilterActions'

const middlewares = [ thunk ]
const mockStore = configureMockStore(middlewares)

describe('FilterActions', () => {
  let store

  beforeEach(() => {
    store = mockStore({
      filter: {
        selectedFilters: [ 'filter1', 'filter2', 'filter3' ]
      }
    })
  })

  describe('loadFilters', () => {
    let sandbox
    let getFiltersStub
    let setErrorSpy

    beforeEach(() => {
      sandbox = sinon.sandbox.create()
      getFiltersStub = sandbox.stub(ProductsGalleryApi, 'getFilters')
      setErrorSpy = sandbox.spy(AppActions, 'setError')
    })

    afterEach(() => {
      sandbox.restore()
    })

    describe('when there are no errors', () => {
      beforeEach(() => {
        getFiltersStub.returns(Promise.resolve('filterList'))
      })

      it('should return action which will set the filter list', (done) => {
        FilterActions.loadFilters(store.dispatch)

        setTimeout(() => {
          expect(store.getActions()).toContain({
            type: SET_FILTER_LIST,
            filterList: 'filterList'
          })

          done()
        })
      })
    })

    describe('when there are errors', () => {
      beforeEach(() => {
        getFiltersStub.returns(Promise.reject('error'))
      })

      it('should call setError', (done) => {
        FilterActions.loadFilters(store.dispatch)

        setTimeout(() => {
          expect(setErrorSpy.calledWith('error')).toBe(true)

          done()
        })
      })
    })
  })

  describe('addFilter', () => {
    it('should return action which will add a selected filter if it does not exist', () => {
      store.dispatch(FilterActions.addFilter('filter4'))

      expect(store.getActions()).toContain({
        type: SET_SELECTED_FILTERS,
        selectedFilters: [ 'filter1', 'filter2', 'filter3', 'filter4' ]
      })
    })

    it('should return action which will keep the selected filters if it exists', () => {
      store.dispatch(FilterActions.addFilter('filter1'))

      expect(store.getState().filter.selectedFilters).toEqual([ 'filter1', 'filter2', 'filter3' ])
    })
  })

  describe('removeFilter', () => {
    it('should return action which will remove a selected filter if it exists', () => {
      store.dispatch(FilterActions.removeFilter('filter2'))

      expect(store.getActions()).toContain({
        type: SET_SELECTED_FILTERS,
        selectedFilters: [ 'filter1', 'filter3' ]
      })
    })

    it('should return action which will keep the selected filters if it does not exist', () => {
      store.dispatch(FilterActions.removeFilter('wrong-filter'))

      expect(store.getState().filter.selectedFilters).toEqual([ 'filter1', 'filter2', 'filter3' ])
    })
  })

  describe('resetFilter', () => {
    it('should return action which will reset the selected filters', () => {
      store.dispatch(FilterActions.resetFilter())

      expect(store.getActions()).toContain({
        type: SET_SELECTED_FILTERS,
        selectedFilters: []
      })
    })
  })
})
