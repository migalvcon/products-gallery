import React from 'react'
import expect from 'expect'
import { shallow } from 'enzyme'
import { createStore } from 'redux'
import sinon from 'sinon'

import {
  FILTER_IDS,
  MIN_WIDTH,
  TITLE
} from '../../src/filter/FilterConstants'

import * as FilterSelectors from '../../src/filter/FilterSelectors'

import CheckBox from '../../src/form/checkBox/CheckBox'
import Filter, { Filter as FilterComponent } from '../../src/filter/Filter'

describe('Filter', () => {
  let props

  beforeEach(() => {
    props = {
      filterList: [
        {
          id: 'filter1',
          name: 'Filter1'
        },
        {
          id: 'filter2',
          name: 'Filter2'
        },
        {
          id: 'filter3',
          name: 'Filter3'
        }
      ],
      selectedFilters: [ 'filter1', 'filter2' ]
    }
  })

  describe('by default', () => {
    let store
    let sandbox
    let wrapper
    let filterListSelectorStub
    let selectedFiltersSelectorStub

    beforeEach(() => {
      sandbox = sinon.sandbox.create()
      filterListSelectorStub = sandbox.stub(FilterSelectors, 'filterListSelector').returns(props.filterList)
      selectedFiltersSelectorStub = sandbox.stub(FilterSelectors, 'selectedFiltersSelector').returns(props.selectedFilters)

      store = createStore(() => ({}))

      wrapper = shallow(<Filter store={store} />)
    })

    afterEach(() => {
      sandbox.restore()
    })

    it('should contain filterList', () => {
      expect(wrapper.props('store').filterList).toEqual(props.filterList)
    })

    it('should contain selectedFilters', () => {
      expect(wrapper.props('store').selectedFilters).toEqual(props.selectedFilters)
    })

    it('should call filterListSelector', () => {
      expect(filterListSelectorStub.calledWith(store.getState())).toBe(true)
    })

    it('should call selectedFiltersSelector', () => {
      expect(selectedFiltersSelectorStub.calledWith(store.getState())).toBe(true)
    })
  })

  describe('component', () => {
    describe('by default', () => {
      let wrapper

      beforeEach(() => {
        wrapper = shallow(<FilterComponent {...props} />)
      })

      it('should contain one CheckBox per filter', () => {
        const elems = wrapper.find(CheckBox)

        expect(elems.length).toEqual(props.filterList.length)
      })

      it('should contain one checked CheckBox per selected filter', () => {
        const elems = wrapper.find(CheckBox).filterWhere(elem => elem.prop('checked'))

        expect(elems.length).toEqual(props.selectedFilters.length)
      })
    })

    describe('if width is enough', () => {
      let wrapper

      beforeEach(() => {
        wrapper = shallow(<FilterComponent {...props} />)
        wrapper.setState({ windowWidth: MIN_WIDTH + 1 })
      })

      it('should contain the title', () => {
        const elem = wrapper.find(`#${FILTER_IDS.TITLE}`)

        expect(elem.text()).toEqual(TITLE)
      })
    })

    describe('if width is not enough', () => {
      let wrapper

      beforeEach(() => {
        wrapper = shallow(<FilterComponent {...props} />)
        wrapper.setState({ windowWidth: MIN_WIDTH - 1 })
      })

      it('should not contain the title', () => {
        expect(wrapper).toNotHaveElement(`#${FILTER_IDS.TITLE}`)
      })
    })

    describe('when a checked CheckBox is clicked', () => {
      let wrapper
      let sandbox
      let addFilterSpy
      let removeFilterSpy

      beforeEach(() => {
        sandbox = sinon.sandbox.create()
        addFilterSpy = sandbox.spy()
        removeFilterSpy = sandbox.spy()

        const newProps = {
          ...props,
          addFilter: addFilterSpy,
          removeFilter: removeFilterSpy
        }

        wrapper = shallow(<FilterComponent {...newProps} />)
      })

      afterEach(() => {
        sandbox.restore()
      })

      it('should remove the selected filter', () => {
        const elem = wrapper.find(CheckBox)
          .filterWhere(elem => elem.prop('checked')).first()
        const filter = elem.prop('name')

        elem.parent().simulate('click')

        expect(removeFilterSpy.calledWith(filter)).toBe(true)
      })
    })

    describe('when a unchecked CheckBox is clicked', () => {
      let wrapper
      let sandbox
      let addFilterSpy
      let removeFilterSpy

      beforeEach(() => {
        sandbox = sinon.sandbox.create()
        addFilterSpy = sandbox.spy()
        removeFilterSpy = sandbox.spy()

        const newProps = {
          ...props,
          addFilter: addFilterSpy,
          removeFilter: removeFilterSpy
        }

        wrapper = shallow(<FilterComponent {...newProps} />)
      })

      afterEach(() => {
        sandbox.restore()
      })

      it('should add the selected filter', () => {
        const elem = wrapper.find(CheckBox)
          .filterWhere(elem => !elem.prop('checked')).first()
        const filter = elem.prop('name')

        elem.parent().simulate('click')

        expect(addFilterSpy.calledWith(filter)).toBe(true)
      })
    })
  })
})
