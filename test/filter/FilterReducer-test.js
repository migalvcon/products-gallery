import expect from 'expect'

import FilterReducer from '../../src/filter/FilterReducer'

describe('FilterReducer', () => {
  let initState
  let endState

  beforeEach(() => {
    initState = FilterReducer({}, { type: 'NOOP' })
  })

  describe('SET_FILTER_LIST', () => {
    let filterList

    beforeEach(() => {
      filterList = [ 'filter1', 'filter2', 'filter3' ]

      const action = {
        type: 'SET_FILTER_LIST',
        filterList
      }

      endState = FilterReducer(initState, action)
    })

    it('should set filterList', () => {
      expect(endState.filterList).toEqual(filterList)
    })
  })

  describe('SET_SELECTED_FILTERS', () => {
    let selectedFilters

    beforeEach(() => {
      selectedFilters = [ 'filter1', 'filter2' ]

      const action = {
        type: 'SET_SELECTED_FILTERS',
        selectedFilters
      }

      endState = FilterReducer(initState, action)
    })

    it('should set selectedFilters', () => {
      expect(endState.selectedFilters).toEqual(selectedFilters)
    })
  })
})
