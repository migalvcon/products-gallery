import expect from 'expect'

import * as AppSelectors from '../src/AppSelectors'

describe('AppSelectors', () => {
  let state

  let error
  let ready

  beforeEach(() => {
    error = 'error'
    ready = true

    state = {
      app: {
        error: error,
        ready: ready
      }
    }
  })

  describe('errorSelector', () => {
    it('should retrieve the error state', () => {
      expect(AppSelectors.errorSelector(state)).toEqual(error)
    })
  })

  describe('readySelector', () => {
    it('should retrieve the ready state', () => {
      expect(AppSelectors.readySelector(state)).toEqual(ready)
    })
  })
})
