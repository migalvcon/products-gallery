import expect from 'expect'

import AppReducer from '../src/AppReducer'

describe('AppReducer', () => {
  let initState
  let endState

  beforeEach(() => {
    initState = AppReducer({}, { type: 'NOOP' })
  })

  describe('SET_ERROR', () => {
    let error

    beforeEach(() => {
      error = 'error'

      const action = {
        type: 'SET_ERROR',
        error
      }

      endState = AppReducer(initState, action)
    })

    it('should set error', () => {
      expect(endState.error).toEqual(error)
    })
  })

  describe('SET_READY', () => {
    let ready

    beforeEach(() => {
      ready = true

      const action = {
        type: 'SET_READY',
        ready
      }

      endState = AppReducer(initState, action)
    })

    it('should set ready', () => {
      expect(endState.ready).toEqual(ready)
    })
  })
})
