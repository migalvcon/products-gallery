import React from 'react'
import expect from 'expect'
import { shallow } from 'enzyme'

import CheckBox from '../../../src/form/checkBox/CheckBox'

describe('CheckBox', () => {
  describe('by default', () => {
    let props
    let wrapper

    beforeEach(() => {
      props = {
        idPrefix: 'idPrefix',
        label: 'label',
        name: 'name'
      }

      wrapper = shallow(<CheckBox {...props} />)
    })

    it('should contain input', () => {
      expect(wrapper).toHaveElement('input')
    })

    it('input checked should be false', () => {
      expect(wrapper.find('input').node.props.checked).toEqual(false)
    })

    it('input name should be props.name', () => {
      expect(wrapper.find('input').node.props.name).toEqual(props.name)
    })

    it('should contain label', () => {
      expect(wrapper).toHaveElement('label')
    })

    it('label text should be props.label', () => {
      expect(wrapper.find('label').text()).toEqual(props.label)
    })
  })

  describe('when is checked', () => {
    let props
    let wrapper

    beforeEach(() => {
      props = {
        idPrefix: 'idPrefix',
        label: 'label',
        name: 'name',
        checked: true
      }

      wrapper = shallow(<CheckBox {...props} />)
    })

    it('input checked should be false', () => {
      expect(wrapper.find('input').node.props.checked).toEqual(true)
    })
  })
})
