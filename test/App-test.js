import React from 'react'
import { GatewayProvider, GatewayDest } from 'react-gateway'
import expect from 'expect'
import { shallow } from 'enzyme'
import { createStore } from 'redux'
import sinon from 'sinon'

import * as AppSelectors from '../src/AppSelectors'

import App, { App as AppComponent} from '../src/App'
import Message from '../src/common/message/Message'
import Spinner from '../src/common/modal/spinner/Spinner'
import Content from '../src/Content'

describe('<App />', () => {
  let props

  beforeEach(() => {
    props = {
      error: 'error',
      ready: true
    }
  })

  describe('by default', () => {
    let store
    let sandbox
    let wrapper
    let errorSelectorStub
    let readySelectorStub

    beforeEach(() => {
      sandbox = sinon.sandbox.create()
      errorSelectorStub = sandbox.stub(AppSelectors, 'errorSelector').returns(props.error)
      readySelectorStub = sandbox.stub(AppSelectors, 'readySelector').returns(props.ready)

      store = createStore(() => ({}))

      wrapper = shallow(<App store={store} />)
    })

    afterEach(() => {
      sandbox.restore()
    })

    it('should contain error', () => {
      expect(wrapper.props('store').error).toEqual(props.error)
    })

    it('should contain ready', () => {
      expect(wrapper.props('store').ready).toEqual(props.ready)
    })

    it('should call errorSelector', () => {
      expect(errorSelectorStub.calledWith(store.getState())).toBe(true)
    })

    it('should call readySelector', () => {
      expect(readySelectorStub.calledWith(store.getState())).toBe(true)
    })
  })

  describe('component', () => {
    let wrapper

    beforeEach(() => {
      wrapper = shallow(<AppComponent {...props} />)
    })

    it('should contain GatewayDest', () => {
      expect(wrapper).toHaveElement(GatewayDest)
    })

    it('should contain GatewayProvider', () => {
      expect(wrapper).toHaveElement(GatewayProvider)
    })

    it('should contain Content', () => {
      expect(wrapper).toHaveElement(Content)
    })

    it('Content show should be props.ready', () => {
      expect(wrapper.find(Content).prop('show')).toBe(props.ready)
    })

    it('should contain Message', () => {
      expect(wrapper).toHaveElement(Message)
    })

    it('Message text should be props.error', () => {
      expect(wrapper.find(Message).prop('text')).toBe(props.error)
    })

    it('should contain Spinner', () => {
      expect(wrapper).toHaveElement(Spinner)
    })

    it('Spinner isEnabled should be !props.ready', () => {
      expect(wrapper.find(Spinner).prop('isEnabled')).toBe(!props.ready)
    })
  })
})
