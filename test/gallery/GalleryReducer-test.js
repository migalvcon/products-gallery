import expect from 'expect'

import GalleryReducer from '../../src/gallery/GalleryReducer'

describe('GalleryReducer', () => {
  let initState
  let endState

  beforeEach(() => {
    initState = GalleryReducer({}, { type: 'NOOP' })
  })

  describe('SET_PRODUCTS', () => {
    let products

    beforeEach(() => {
      products = [ 'product1', 'product2', 'product3' ]

      const action = {
        type: 'SET_PRODUCTS',
        products
      }

      endState = GalleryReducer(initState, action)
    })

    it('should set products', () => {
      expect(endState.products).toEqual(products)
    })
  })
})
