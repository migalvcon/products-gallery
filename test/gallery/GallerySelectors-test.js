import expect from 'expect'

import * as GallerySelectors from '../../src/gallery/GallerySelectors'

describe('GallerySelectors', () => {
  let state

  let products

  beforeEach(() => {
    products = [ 'product1', 'product2', 'product3' ]

    state = {
      gallery: {
        products: products
      }
    }
  })

  describe('productsSelector', () => {
    it('should retrieve the products state', () => {
      expect(GallerySelectors.productsSelector(state)).toEqual(products)
    })
  })
})
