import React from 'react'
import LinesEllipsis from 'react-lines-ellipsis'
import expect from 'expect'
import { shallow } from 'enzyme'
import { createStore } from 'redux'
import sinon from 'sinon'

import {
  GALLERY_IDS
} from '../../src/gallery/GalleryConstants'

import * as FilterSelectors from '../../src/filter/FilterSelectors'

import Product, { Product as ProductComponent } from '../../src/gallery/Product'

describe('Product', () => {
  let props

  beforeEach(() => {
    props = {
      filterList: [
        {
          id: 'filter1',
          name: 'Filter1'
        },
        {
          id: 'filter2',
          name: 'Filter2'
        },
        {
          id: 'filter3',
          name: 'Filter3'
        }
      ],
      product: {
        category: 'filter2',
        description: 'description',
        image: 'image',
        name: 'name'
      }
    }
  })

  describe('by default', () => {
    let store
    let sandbox
    let wrapper
    let filterListSelectorStub

    beforeEach(() => {
      sandbox = sinon.sandbox.create()
      filterListSelectorStub = sandbox.stub(FilterSelectors, 'filterListSelector').returns(props.filterList)

      store = createStore(() => ({}))

      wrapper = shallow(<Product store={store} {...props} />)
    })

    afterEach(() => {
      sandbox.restore()
    })

    it('should contain filterList', () => {
      expect(wrapper.props('store').filterList).toEqual(props.filterList)
    })

    it('should contain product', () => {
      expect(wrapper.props('store').product).toEqual(props.product)
    })

    it('should call filterListSelector', () => {
      expect(filterListSelectorStub.calledWith(store.getState())).toBe(true)
    })
  })

  describe('component', () => {
    let wrapper

    beforeEach(() => {
      wrapper = shallow(<ProductComponent {...props} />)
    })

    it('should contain product image', () => {
      const image = wrapper.find('img')

      expect(image.prop('src')).toEqual(props.product.image)
    })

    it('should contain product name', () => {
      const elem = wrapper.find(`#${GALLERY_IDS.PRODUCT_NAME}`)

      expect(elem.text()).toEqual(props.product.name)
    })

    it('should contain product description', () => {
      const elem = wrapper.find(LinesEllipsis)

      expect(elem.prop('text')).toEqual(props.product.description)
    })

    it('should contain product category', () => {
      const elem = wrapper.find(`#${GALLERY_IDS.PRODUCT_CATEGORY}`)

      expect(elem.text()).toEqual('Filter2')
    })
  })
})
