import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import expect from 'expect'
import sinon from 'sinon'

import {
  SET_PRODUCTS
} from '../../src/gallery/GalleryConstants'

import * as ProductsGalleryApi from '../../src/api/ProductsGalleryApi'

import * as AppActions from '../../src/AppActions'
import * as GalleryActions from '../../src/gallery/GalleryActions'

const middlewares = [ thunk ]
const mockStore = configureMockStore(middlewares)

describe('GalleryActions', () => {
  let store

  beforeEach(() => {
    store = mockStore({})
  })

  describe('loadProducts', () => {
    let sandbox
    let getProductsStub
    let setErrorSpy

    beforeEach(() => {
      sandbox = sinon.sandbox.create()
      getProductsStub = sandbox.stub(ProductsGalleryApi, 'getProducts')
      setErrorSpy = sandbox.spy(AppActions, 'setError')
    })

    afterEach(() => {
      sandbox.restore()
    })

    describe('when there are no errors', () => {
      beforeEach(() => {
        getProductsStub.returns(Promise.resolve('products'))
      })

      it('should return action which will set the filter list', (done) => {
        GalleryActions.loadProducts(store.dispatch)

        setTimeout(() => {
          expect(store.getActions()).toContain({
            type: SET_PRODUCTS,
            products: 'products'
          })

          done()
        })
      })
    })

    describe('when there are errors', () => {
      beforeEach(() => {
        getProductsStub.returns(Promise.reject('error'))
      })

      it('should call setError', (done) => {
        GalleryActions.loadProducts(store.dispatch)

        setTimeout(() => {
          expect(setErrorSpy.calledWith('error')).toBe(true)

          done()
        })
      })
    })
  })
})
