import React from 'react'
import expect from 'expect'
import { shallow } from 'enzyme'
import { createStore } from 'redux'
import sinon from 'sinon'

import * as FilterSelectors from '../../src/filter/FilterSelectors'
import * as GallerySelectors from '../../src/gallery/GallerySelectors'
import * as TopBarSelectors from '../../src/topBar/TopBarSelectors'

import Gallery, { Gallery as GalleryComponent } from '../../src/gallery/Gallery'
import Product from '../../src/gallery/Product'

describe('Gallery', () => {
  let props

  beforeEach(() => {
    props = {
      itemSelected: 'menu1',
      selectedFilters: [ 'filter1', 'filter2' ],
      products: [
        {
          category: 'filter1',
          description: 'description1',
          image: 'image1',
          name: 'name1',
          menu: 'menu1'
        },
        {
          category: 'filter2',
          description: 'description2',
          image: 'image2',
          name: 'name2',
          menu: 'menu2'
        },
        {
          category: 'filter3',
          description: 'description3',
          image: 'image3',
          name: 'name3',
          menu: 'menu3'
        }
      ]
    }
  })

  describe('by default', () => {
    let store
    let sandbox
    let wrapper
    let itemSelectedSelectorStub
    let selectedFiltersSelectorStub
    let productsSelectorStub

    beforeEach(() => {
      sandbox = sinon.sandbox.create()
      itemSelectedSelectorStub = sandbox.stub(TopBarSelectors, 'itemSelectedSelector').returns(props.itemSelected)
      selectedFiltersSelectorStub = sandbox.stub(FilterSelectors, 'selectedFiltersSelector').returns(props.selectedFilters)
      productsSelectorStub = sandbox.stub(GallerySelectors, 'productsSelector').returns(props.products)

      store = createStore(() => ({}))

      wrapper = shallow(<Gallery store={store} />)
    })

    afterEach(() => {
      sandbox.restore()
    })

    it('should contain products filtered by menu and categories', () => {
      expect(wrapper.props('store').products).toEqual([
        {
          category: 'filter1',
          description: 'description1',
          image: 'image1',
          name: 'name1',
          menu: 'menu1'
        }
      ])
    })

    it('should call itemSelectedSelector', () => {
      expect(itemSelectedSelectorStub.calledWith(store.getState())).toBe(true)
    })

    it('should call selectedFiltersSelector', () => {
      expect(selectedFiltersSelectorStub.calledWith(store.getState())).toBe(true)
    })

    it('should call productsSelector', () => {
      expect(productsSelectorStub.calledWith(store.getState())).toBe(true)
    })
  })

  describe('component', () => {
    let wrapper
    let newProps

    beforeEach(() => {
      newProps = {
        products: props.products
      }

      wrapper = shallow(<GalleryComponent {...newProps} />)
    })

    it('should contain one Product per product', () => {
      const elems = wrapper.find(Product)

      expect(elems.length).toEqual(newProps.products.length)
    })

    it('each Product should contain a product', () => {
      let pos = -1

      const elems = wrapper.find(Product)

      expect(elems.everyWhere(elem => {
        pos++

        return elem.prop('product') === newProps.products[pos]
      })).toBe(true)
    })
  })
})
