import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import expect from 'expect'
import sinon from 'sinon'

import * as CommonHelpers from '../../src/common/CommonHelpers'

import * as AppActions from '../../src/AppActions'
import * as ProductsGalleryApi from '../../src/api/ProductsGalleryApi'

const middlewares = [ thunk ]
const mockStore = configureMockStore(middlewares)

describe('ProductsGalleryApi', () => {
  let store
  let sandbox
  let sleepStub
  let setErrorSpy

  beforeEach(() => {
    store = mockStore({})

    sandbox = sinon.sandbox.create()
    sleepStub = sandbox.stub(CommonHelpers, 'sleep')
    setErrorSpy = sandbox.spy(AppActions, 'setError')
  })

  afterEach(() => {
    sandbox.restore()
  })

  describe('getProducts', () => {
    describe('when there are no errors', () => {
      beforeEach(() => {
        sleepStub.returns(Promise.resolve())
      })

      it('should return a Promise resolved', (done) => {
        const promise = ProductsGalleryApi.getProducts(store.dispatch)

        setTimeout(() => {
          promise.then(response => {
            expect(response).toBe(Promise.resolve(sinon.match.typeOf('object')))
          })

          done()
        })
      })

      it('should not call setError', (done) => {
        ProductsGalleryApi.getProducts(store.dispatch)

        setTimeout(() => {
          expect(setErrorSpy.called).toBe(false)

          done()
        })
      })
    })

    describe('when there are errors', () => {
      beforeEach(() => {
        sleepStub.returns(Promise.reject('error'))
      })

      it('should return a Promise reject', (done) => {
        const promise = ProductsGalleryApi.getProducts(store.dispatch)

        setTimeout(() => {
          promise.then(response => {
            expect(response).toBe(Promise.reject(sinon.match.typeOf('string')))
          })

          done()
        })
      })

      it('should call setError', (done) => {
        ProductsGalleryApi.getProducts(store.dispatch)

        setTimeout(() => {
          expect(setErrorSpy.calledWith('error')).toBe(true)

          done()
        })
      })
    })
  })

  describe('getFilters', () => {
    describe('when there are no errors', () => {
      beforeEach(() => {
        sleepStub.returns(Promise.resolve())
      })

      it('should return a Promise resolved', (done) => {
        const promise = ProductsGalleryApi.getFilters(store.dispatch)

        setTimeout(() => {
          promise.then(response => {
            expect(response).toBe(Promise.resolve(sinon.match.typeOf('object')))
          })

          done()
        })
      })

      it('should not call setError', (done) => {
        ProductsGalleryApi.getFilters(store.dispatch)

        setTimeout(() => {
          expect(setErrorSpy.called).toBe(false)

          done()
        })
      })
    })

    describe('when there are errors', () => {
      beforeEach(() => {
        sleepStub.returns(Promise.reject('error'))
      })

      it('should return a Promise reject', (done) => {
        const promise = ProductsGalleryApi.getFilters(store.dispatch)

        setTimeout(() => {
          promise.then(response => {
            expect(response).toBe(Promise.reject(sinon.match.typeOf('string')))
          })

          done()
        })
      })

      it('should call setError', (done) => {
        ProductsGalleryApi.getFilters(store.dispatch)

        setTimeout(() => {
          expect(setErrorSpy.calledWith('error')).toBe(true)

          done()
        })
      })
    })
  })

  describe('getMenuItems', () => {
    describe('when there are no errors', () => {
      beforeEach(() => {
        sleepStub.returns(Promise.resolve())
      })

      it('should return a Promise resolved', (done) => {
        const promise = ProductsGalleryApi.getMenuItems(store.dispatch)

        setTimeout(() => {
          promise.then(response => {
            expect(response).toBe(Promise.resolve(sinon.match.typeOf('object')))
          })

          done()
        })
      })

      it('should not call setError', (done) => {
        ProductsGalleryApi.getMenuItems(store.dispatch)

        setTimeout(() => {
          expect(setErrorSpy.called).toBe(false)

          done()
        })
      })
    })

    describe('when there are errors', () => {
      beforeEach(() => {
        sleepStub.returns(Promise.reject('error'))
      })

      it('should return a Promise reject', (done) => {
        const promise = ProductsGalleryApi.getMenuItems(store.dispatch)

        setTimeout(() => {
          promise.then(response => {
            expect(response).toBe(Promise.reject(sinon.match.typeOf('string')))
          })

          done()
        })
      })

      it('should call setError', (done) => {
        ProductsGalleryApi.getMenuItems(store.dispatch)

        setTimeout(() => {
          expect(setErrorSpy.calledWith('error')).toBe(true)

          done()
        })
      })
    })
  })
})
