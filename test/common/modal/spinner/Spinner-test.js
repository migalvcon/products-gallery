import React from 'react'
import expect from 'expect'
import { shallow } from 'enzyme'

import Modal from '../../../../src/common/modal/Modal'
import Spinner from '../../../../src/common/modal/spinner/Spinner'

describe('Spinner', () => {
  describe('by default', () => {
    let wrapper
    let props

    beforeEach(() => {
      props = {
        idPrefix: 'idPrefix',
        isEnabled: true,
        loadingMessage: 'loadingMessage'
      }

      wrapper = shallow(<Spinner {...props} />)
    })

    it('should contain the loading message', () => {
      expect(wrapper.find(`#${props.idPrefix}-loading-message`).text()).toEqual(props.loadingMessage)
    })

    it('should contain Modal with isEnabled', () => {
      expect(wrapper).toHaveElement(Modal)
    })

    it('Modal isEnabled should be props.isEnabled', () => {
      expect(wrapper.find(Modal).prop('isEnabled')).toBe(props.isEnabled)
    })
  })
})
