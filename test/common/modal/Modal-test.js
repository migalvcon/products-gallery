import React from 'react'
import ReactModal2 from 'react-modal2'
import expect from 'expect'
import { shallow } from 'enzyme'

import Modal from '../../../src/common/modal/Modal'

describe('Modal', () => {
  describe('when isEnabled is false', () => {
    let props
    let wrapper

    beforeEach(() => {
      props = {
        isEnabled: false
      }

      wrapper = shallow(<Modal {...props} />)
    })

    it('should return empty', () => {
      expect(wrapper.node).toNotExist()
    })
  })

  describe('when isEnabled is true', () => {
    let props
    let wrapper

    beforeEach(() => {
      props = {
        isEnabled: true
      }

      wrapper = shallow(<Modal {...props} />)
    })

    it('should contain ReactModal2', () => {
      expect(wrapper).toHaveElement(ReactModal2)
    })
  })

  describe('when has children', () => {
    let props
    let wrapper

    beforeEach(() => {
      props = {
        isEnabled: true
      }

      wrapper = shallow(<Modal {...props}><div id="children">children</div></Modal>)
    })

    it('should contain children', () => {
      expect(wrapper).toHaveElement('#children')
    })
  })
})
