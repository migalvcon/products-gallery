import React from 'react'
import expect from 'expect'
import { shallow } from 'enzyme'

import Message from '../../../src/common/message/Message'

import errorIcon from '../../../src/common/message/images/error-icon.png'

describe('Message', () => {
  describe('when the text is empty', () => {
    let props
    let wrapper

    beforeEach(() => {
      props = {
        idPrefix: 'idPrefix',
        text: ''
      }

      wrapper = shallow(<Message {...props} />)
    })

    it('should return empty', () => {
      expect(wrapper.node).toNotExist()
    })
  })

  describe('when the text is not empty', () => {
    let props
    let wrapper

    beforeEach(() => {
      props = {
        idPrefix: 'idPrefix',
        text: 'text'
      }

      wrapper = shallow(<Message {...props} />)
    })

    it('should contain errorIcon', () => {
      expect(wrapper.find('img').prop('src')).toEqual(errorIcon)
    })

    it('should contain the text', () => {
      expect(wrapper.find('span').text()).toBe(props.text)
    })
  })
})
