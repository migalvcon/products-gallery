import expect from 'expect'

import * as CommonHelpers from '../../src/common/CommonHelpers'

describe('CommonHelpers', () => {
  describe('byValue', () => {
    const attribute = 'key'
    const itemA = {
      key: 'value1'
    }
    const itemB = {
      key: 'value1'
    }
    const itemC = {
      key: 'value2'
    }
    const itemNull = {
      key: null
    }

    it('should filter items by value', () => {
      expect([ itemA, itemB, itemC, itemNull ].filter(CommonHelpers.byValue('value1', attribute))).toEqual([ itemA, itemB ])
    })
  })

  describe('byValueArray', () => {
    const attribute = 'key'
    const itemA = {
      key: 'value1'
    }
    const itemB = {
      key: 'value1'
    }
    const itemC = {
      key: 'value2'
    }
    const itemNull = {
      key: null
    }

    it('should filter items by array of values', () => {
      expect([ itemA, itemB, itemC, itemNull ].filter(CommonHelpers.byValueArray([ 'value1', 'value2' ], attribute))).toEqual([ itemA, itemB, itemC ])
    })
  })
})
