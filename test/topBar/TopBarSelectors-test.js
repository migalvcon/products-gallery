import expect from 'expect'

import * as TopBarSelectors from '../../src/topBar/TopBarSelectors'

describe('TopBarSelectors', () => {
  let state

  let itemSelected
  let menuItems

  beforeEach(() => {
    itemSelected = 'item1'
    menuItems = [ 'menuItem1', 'menuItem2' ]

    state = {
      topBar: {
        itemSelected: itemSelected,
        menuItems: menuItems
      }
    }
  })

  describe('itemSelectedSelector', () => {
    it('should retrieve the itemSelected state', () => {
      expect(TopBarSelectors.itemSelectedSelector(state)).toEqual(itemSelected)
    })
  })

  describe('menuItemsSelector', () => {
    it('should retrieve the menuItems state', () => {
      expect(TopBarSelectors.menuItemsSelector(state)).toEqual(menuItems)
    })
  })
})
