import expect from 'expect'

import TopBarReducer from '../../src/topBar/TopBarReducer'

describe('TopBarReducer', () => {
  let initState
  let endState

  beforeEach(() => {
    initState = TopBarReducer({}, { type: 'NOOP' })
  })

  describe('SET_ITEM_SELECTED', () => {
    let itemSelected

    beforeEach(() => {
      itemSelected = 'item1'

      const action = {
        type: 'SET_ITEM_SELECTED',
        itemSelected
      }

      endState = TopBarReducer(initState, action)
    })

    it('should set itemSelected', () => {
      expect(endState.itemSelected).toEqual(itemSelected)
    })
  })

  describe('SET_MENU_ITEMS', () => {
    let menuItems

    beforeEach(() => {
      menuItems = [ 'menuItem1', 'menuItem2' ]

      const action = {
        type: 'SET_MENU_ITEMS',
        menuItems
      }

      endState = TopBarReducer(initState, action)
    })

    it('should set menuItems', () => {
      expect(endState.menuItems).toEqual(menuItems)
    })
  })
})
