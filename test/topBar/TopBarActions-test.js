import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import expect from 'expect'
import sinon from 'sinon'

import {
  SET_ITEM_SELECTED,
  SET_MENU_ITEMS
} from '../../src/topBar/TopBarConstants'

import TopBarReducer from '../../src/topBar/TopBarReducer'

import * as ProductsGalleryApi from '../../src/api/ProductsGalleryApi'

import * as AppActions from '../../src/AppActions'
import * as FilterActions from '../../src/filter/FilterActions'
import * as TopBarActions from '../../src/topBar/TopBarActions'

const middlewares = [ thunk ]
const mockStore = configureMockStore(middlewares)

describe('TopBarActions', () => {
  let initialState
  let store

  beforeEach(() => {
    initialState = TopBarReducer(undefined, { type: 'NOOP' })
    store = mockStore(initialState)
  })

  describe('loadMenuItems', () => {
    let sandbox
    let getMenuItemsStub
    let setErrorSpy

    beforeEach(() => {
      sandbox = sinon.sandbox.create()
      getMenuItemsStub = sandbox.stub(ProductsGalleryApi, 'getMenuItems')
      setErrorSpy = sandbox.spy(AppActions, 'setError')
    })

    afterEach(() => {
      sandbox.restore()
    })

    describe('when there are no errors', () => {
      beforeEach(() => {
        getMenuItemsStub.returns(Promise.resolve('menuItems'))
      })

      it('should return action which will set the filter list', (done) => {
        TopBarActions.loadMenuItems(store.dispatch)

        setTimeout(() => {
          expect(store.getActions()).toContain({
            type: SET_MENU_ITEMS,
            menuItems: 'menuItems'
          })

          done()
        })
      })
    })

    describe('when there are errors', () => {
      beforeEach(() => {
        getMenuItemsStub.returns(Promise.reject('error'))
      })

      it('should call setError', (done) => {
        TopBarActions.loadMenuItems(store.dispatch)

        setTimeout(() => {
          expect(setErrorSpy.calledWith('error')).toBe(true)

          done()
        })
      })
    })
  })

  describe('selectMenuItem', () => {
    let sandbox
    let resetFilterSpy

    beforeEach(() => {
      sandbox = sinon.sandbox.create()
      resetFilterSpy = sandbox.spy(FilterActions, 'resetFilter')
    })

    afterEach(() => {
      sandbox.restore()
    })

    it('should return action which will set the item selected', () => {
      store.dispatch(TopBarActions.selectMenuItem('menuItem'))

      expect(store.getActions()).toContain({
        type: SET_ITEM_SELECTED,
        itemSelected: 'menuItem'
      })
    })

    it('should call resetFilter', () => {
      store.dispatch(TopBarActions.selectMenuItem('menuItem'))

      expect(resetFilterSpy.calledWith()).toBe(true)
    })
  })
})
