import React from 'react'
import expect from 'expect'
import { shallow } from 'enzyme'
import { createStore } from 'redux'
import sinon from 'sinon'

import {
  MIN_WIDTH,
  TOP_BAR_IDS
} from '../../src/topBar/TopBarConstants'

import * as TopBarSelectors from '../../src/topBar/TopBarSelectors'

import TopBar, { TopBar as TopBarComponent } from '../../src/topBar/TopBar'

describe('TopBar', () => {
  let props

  beforeEach(() => {
    props = {
      itemSelected: 'menuItem1',
      menuItems: [
        {
          id: 'menuItem1',
          name: 'Menu Item 1'
        },
        {
          id: 'menuItem2',
          name: 'Menu Item 2'
        },
        {
          id: 'menuItem3',
          name: 'Menu Item 3'
        }
      ]
    }
  })

  describe('by default', () => {
    let store
    let sandbox
    let wrapper
    let itemSelectedSelectorStub
    let menuItemsSelectorStub

    beforeEach(() => {
      sandbox = sinon.sandbox.create()
      itemSelectedSelectorStub = sandbox.stub(TopBarSelectors, 'itemSelectedSelector').returns(props.itemSelected)
      menuItemsSelectorStub = sandbox.stub(TopBarSelectors, 'menuItemsSelector').returns(props.menuItems)

      store = createStore(() => ({}))

      wrapper = shallow(<TopBar store={store} />)
    })

    afterEach(() => {
      sandbox.restore()
    })

    it('should contain itemSelected', () => {
      expect(wrapper.props('store').itemSelected).toEqual(props.itemSelected)
    })

    it('should contain menuItems', () => {
      expect(wrapper.props('store').menuItems).toEqual(props.menuItems)
    })

    it('should call itemSelectedSelector', () => {
      expect(itemSelectedSelectorStub.calledWith(store.getState())).toBe(true)
    })

    it('should call menuItemsSelector', () => {
      expect(menuItemsSelectorStub.calledWith(store.getState())).toBe(true)
    })
  })

  describe('component', () => {
    describe('if width is enough', () => {
      let wrapper

      beforeEach(() => {
        wrapper = shallow(<TopBarComponent {...props} />)
        wrapper.setState({ windowWidth: MIN_WIDTH + 1 })
      })

      it('should contain one div per menu item', () => {
        const elems = wrapper.find(`#${TOP_BAR_IDS.MENU_ITEMS}`)

        expect(elems.children().length).toEqual(props.menuItems.length)
      })
    })

    describe('if width is not enough', () => {
      let wrapper

      beforeEach(() => {
        wrapper = shallow(<TopBarComponent {...props} />)
        wrapper.setState({ windowWidth: MIN_WIDTH - 1 })
      })

      describe('by default', () => {
        it('should contain the menu icon', () => {
          expect(wrapper).toHaveElement('img')
        })

        it('should not contain the menu items', () => {
          expect(wrapper.find(`#${TOP_BAR_IDS.MENU_ITEMS_MOBILE}`).children().length).toEqual(0)
        })

        it('should get the mobileView state', () => {
          expect(wrapper.state('mobileView')).toBe(false)
        })
      })

      describe('when the menu icon is clicked', () => {
        beforeEach(() => {
          const menuIcon = wrapper.find(`#${TOP_BAR_IDS.MENU_ICON}`)
          menuIcon.simulate('click')
        })

        it('should set the mobileView state', () => {
          expect(wrapper.state('mobileView')).toBe(true)
        })

        it('should contain one div per menu item', () => {
          const elems = wrapper.find(`#${TOP_BAR_IDS.MENU_ITEMS_MOBILE}`)

          expect(elems.children().length).toEqual(props.menuItems.length)
        })
      })
    })

    describe('when a menu item is clicked', () => {
      let sandbox
      let wrapper
      let newProps
      let selectMenuItemSpy

      beforeEach(() => {
        sandbox = sinon.sandbox.create()
        selectMenuItemSpy = sandbox.spy()

        newProps = {
          ...props,
          selectMenuItem: selectMenuItemSpy
        }

        wrapper = shallow(<TopBarComponent {...newProps} />)
        wrapper.setState({ windowWidth: MIN_WIDTH + 1 })
      })

      afterEach(() => {
        sandbox.restore()
      })

      it('should set the selected menu item', () => {
        const elem = wrapper.find(`#${TOP_BAR_IDS.MENU_ITEMS}`)
        const menuItem = elem.children().first()

        menuItem.simulate('click')

        expect(selectMenuItemSpy.calledWith(props.menuItems[0].id)).toBe(true)
      })
    })
  })
})
