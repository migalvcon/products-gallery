import React from 'react'
import expect from 'expect'
import { shallow } from 'enzyme'
import { Provider } from 'react-redux'
import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'

import Root from '../src/Root'
import App from '../src/App'

const middlewares = [ thunk ]
const mockStore = configureMockStore(middlewares)

describe('<Root />', () => {
  describe('by default', () => {
    let props
    let wrapper

    beforeEach(() => {
      props = {
        store: mockStore({})
      }

      wrapper = shallow(<Root {...props} />)
    })

    it('should contain Provider', () => {
      expect(wrapper).toHaveElement(Provider)
    })

    it('should contain App', () => {
      expect(wrapper).toHaveElement(App)
    })
  })
})
