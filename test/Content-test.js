import React from 'react'
import expect from 'expect'
import { shallow } from 'enzyme'

import Content from '../src/Content'
import Filter from '../src/filter/Filter'
import Gallery from '../src/gallery/Gallery'
import TopBar from '../src/topBar/TopBar'

describe('<Content />', () => {
  describe('when show is false', () => {
    let wrapper

    beforeEach(() => {
      wrapper = shallow(<Content />)
    })

    it('should return empty', () => {
      expect(wrapper.node).toNotExist()
    })
  })

  describe('when show is true', () => {
    let props
    let wrapper

    beforeEach(() => {
      props = {
        show: true
      }

      wrapper = shallow(<Content {...props} />)
    })

    it('should contain TopBar', () => {
      expect(wrapper).toHaveElement(TopBar)
    })

    it('should contain Filter', () => {
      expect(wrapper).toHaveElement(Filter)
    })

    it('should contain Gallery', () => {
      expect(wrapper).toHaveElement(Gallery)
    })
  })
})
