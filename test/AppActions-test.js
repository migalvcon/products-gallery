import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import expect from 'expect'
import sinon from 'sinon'

import {
  SET_ERROR,
  SET_READY
} from '../src/AppConstants'

import AppReducer from '../src/AppReducer'

import * as AppActions from '../src/AppActions'
import * as FilterActions from '../src/filter/FilterActions'
import * as GalleryActions from '../src/gallery/GalleryActions'
import * as TopBarActions from '../src/topBar/TopBarActions'

const middlewares = [ thunk ]
const mockStore = configureMockStore(middlewares)

describe('AppActions', () => {
  let initialState
  let store

  beforeEach(() => {
    initialState = AppReducer(undefined, { type: 'NOOP' })
    store = mockStore(initialState)
  })

  describe('setError', () => {
    it('should return action which will set the error', () => {
      store.dispatch(AppActions.setError('error'))

      expect(store.getActions()).toContain({
        type: SET_ERROR,
        error: 'error'
      })
    })
  })

  describe('initialize', () => {
    let sandbox
    let loadFiltersStub
    let loadProductsStub
    let loadMenuItemsStub

    beforeEach(() => {
      sandbox = sinon.sandbox.create()
      loadFiltersStub = sandbox.stub(FilterActions, 'loadFilters')
      loadProductsStub = sandbox.stub(GalleryActions, 'loadProducts')
      loadMenuItemsStub = sandbox.stub(TopBarActions, 'loadMenuItems')
    })

    afterEach(() => {
      sandbox.restore()
    })

    describe('by default', () => {
      it('should call loadFilters', () => {
        AppActions.initialize(store.dispatch)

        expect(loadFiltersStub.calledWith()).toBe(true)
      })

      it('should call loadProducts', () => {
        AppActions.initialize(store.dispatch)

        expect(loadProductsStub.calledWith()).toBe(true)
      })

      it('should call loadMenuItems', () => {
        AppActions.initialize(store.dispatch)

        expect(loadMenuItemsStub.calledWith()).toBe(true)
      })
    })

    describe('when there are no errors', () => {
      beforeEach(() => {
        loadFiltersStub.returns(Promise.resolve())
        loadProductsStub.returns(Promise.resolve())
        loadMenuItemsStub.returns(Promise.resolve())
      })

      it('should return action which will set the status', (done) => {
        AppActions.initialize(store.dispatch)

        setTimeout(() => {
          expect(store.getActions()).toContain({
            type: SET_READY,
            ready: true
          })

          done()
        })
      })

      it('should not return action which will set the error', (done) => {
        AppActions.initialize(store.dispatch)

        setTimeout(() => {
          expect(store.getActions()).toNotContain({
            type: SET_ERROR,
            error: sinon.match.typeOf('string')
          })

          done()
        })
      })
    })

    describe('when there are errors', () => {
      beforeEach(() => {
        loadFiltersStub.returns(Promise.reject('error'))
        loadProductsStub.returns(Promise.reject('error'))
        loadMenuItemsStub.returns(Promise.reject('error'))
      })

      it('should not return action which will set the status', (done) => {
        AppActions.initialize(store.dispatch)

        setTimeout(() => {
          expect(store.getActions()).toNotContain({
            type: SET_READY,
            ready: sinon.match.typeOf('boolean')
          })

          done()
        })
      })

      it('should return action which will set the error', (done) => {
        AppActions.initialize(store.dispatch)

        setTimeout(() => {
          expect(store.getActions()).toContain({
            type: SET_ERROR,
            error: 'error'
          })

          done()
        })
      })
    })
  })
})
