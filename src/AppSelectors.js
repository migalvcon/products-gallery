export const errorSelector = (state) => state.app.error

export const readySelector = (state) => state.app.ready
