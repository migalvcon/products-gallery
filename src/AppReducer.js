import {
  SET_ERROR,
  SET_READY
} from './AppConstants'

const initialState = {
  error: '',
  ready: false
}

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_ERROR: {
      return {
        ...state,
        error: action.error
      }
    }
    case SET_READY: {
      return {
        ...state,
        ready: action.ready
      }
    }
    default: {
      return state
    }
  }
}
