import products from '../../data/products.json'
import filters from '../../data/filters.json'
import menuItems from '../../data/menu-items.json'

import {
  sleep
} from '../common/CommonHelpers'

import {
  setError
} from '../AppActions'

// Simulating a real behaviour while retrieving information from server
export const getProducts = (dispatch) =>
  sleep(2000)
  .then(() => Promise.resolve(products))
  .catch(error => {
    dispatch(setError(error))

    return Promise.reject(error)
  })

export const getFilters = (dispatch) =>
  sleep(2000)
  .then(() => Promise.resolve(filters))
  .catch(error => {
    dispatch(setError(error))

    return Promise.reject(error)
  })

export const getMenuItems = (dispatch) =>
  sleep(2000)
  .then(() => Promise.resolve(menuItems))
  .catch(error => {
    dispatch(setError(error))

    return Promise.reject(error)
  })
