import React, { Component } from 'react'
import Radium from 'radium'
import { GatewayProvider, GatewayDest } from 'react-gateway'
import { connect } from 'react-redux'

import {
  APP_IDS,
  MODAL_PLACEMENT
} from './AppConstants'

import {
  errorSelector,
  readySelector
} from './AppSelectors'

import Content from './Content'
import Message from './common/message/Message'
import Spinner from './common/modal/spinner/Spinner'

import styles from './AppStyles'

@Radium
export class App extends Component {
  render() {
    const {
      error,
      ready
    } = this.props

    return (
      <GatewayProvider>
        <div id={APP_IDS.BACKGROUND} style={styles.background}>
          <GatewayDest name={MODAL_PLACEMENT} />
          <Spinner idPrefix={APP_IDS.MODAL} isEnabled={!ready} />
          <Message idPrefix={APP_IDS.NOTIFICATION} text={error}/>
          <Content show={ready} />
        </div>
      </GatewayProvider>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    error: errorSelector(state),
    ready: readySelector(state)
  }
}

export default connect(
  mapStateToProps
) (App)
