import {
  SET_ERROR,
  SET_READY
} from './AppConstants'

import {
  loadFilters
} from './filter/FilterActions'
import {
  loadProducts
} from './gallery/GalleryActions'
import {
  loadMenuItems
} from './topBar/TopBarActions'

const setReady = (ready) => {
  return {
    type: SET_READY,
    ready
  }
}

export const setError = (error) => {
  return {
    type: SET_ERROR,
    error
  }
}

export const initialize = (dispatch) => {
  const dataLoadingPromises = [
    loadFilters(dispatch),
    loadProducts(dispatch),
    loadMenuItems(dispatch)
  ]

  Promise.all(dataLoadingPromises)
  .then(() => dispatch(setReady(true)))
  .catch(error => dispatch(setError(error)))
}
