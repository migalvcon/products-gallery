import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Radium from 'radium'

import Filter from './filter/Filter'
import Gallery from './gallery/Gallery'
import TopBar from './topBar/TopBar'

@Radium
export class Content extends Component {
  render() {
    const {
      show
    } = this.props

    if (!show) {
      return false
    }

    return (
      <div>
        <TopBar />
        <Filter />
        <Gallery />
      </div>
    )
  }
}

Content.propTypes = {
  show: PropTypes.bool
}

Content.defaultProps = {
  show: false
}

export default Content
