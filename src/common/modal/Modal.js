import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Radium from 'radium'
import ReactModal2 from 'react-modal2'
import { Gateway } from 'react-gateway'

import {
  MODAL_PLACEMENT
} from '../../AppConstants'

import styles from './ModalStyles'

ReactModal2.getApplicationElement = () =>
  document.getElementById('root-container')

@Radium
class Modal extends Component {
  render() {
    const {
      isEnabled,
      children,
      onClose
    } = this.props

    if (!isEnabled) {
      return false
    }

    return (
      <Gateway into={MODAL_PLACEMENT}>
        <ReactModal2
          onClose={onClose}
          closeOnEsc={false}
          closeOnBackdropClick={false}
          backdropStyles={styles.mask}
          modalStyles={styles.outline}>
          <div id="modal-content" style={styles.box}>
            {children}
          </div>
        </ReactModal2>
      </Gateway>
    )
  }
}

Modal.PropTypes = {
  isEnabled: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired
}

Modal.defaultProps = {
  onClose: () => {}
}

export default Modal
