import { color } from '../StyleConstants'

const styles = {
  box: {
    backgroundColor: color.white,
    borderRadius: 3,
    boxShadow: '0 0 7px ' + color.blackTransparent,
    display: 'inline-block',
    left: '50%',
    outline: 'none',
    position: 'absolute',
    top: '50%',
    transform: 'translateX(-50%) translateY(-50%)'
  },
  mask: {
    backgroundColor: color.whiteTransparent,
    bottom: 0,
    height: '100%',
    position: 'fixed',
    top: 0,
    right: 0,
    width: '100%',
    zIndex: 1
  },
  outline: {
    outline: '1px solid invert'
  }
}

export default styles
