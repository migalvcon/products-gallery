import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Radium from 'radium'

import styles from './SpinnerStyles'

import loader from './images/loader.gif'

import {
  LOADING_MESSAGE
} from './SpinnerConstants'

import Modal from '../Modal'

@Radium
class Spinner extends Component {
  render() {
    const {
      idPrefix,
      isEnabled,
      loadingMessage
    } = this.props

    return (
      <Modal isEnabled={isEnabled}>
        <div style={styles.loadingMessageBox}>
          <span style={styles.spinner}>
            <img src={loader} />
          </span>
          <div id={`${idPrefix}-loading-message`} style={styles.loadingMessageText}>{loadingMessage}</div>
        </div>
      </Modal>
    )
  }
}

Spinner.PropTypes = {
  idPrefix: PropTypes.string.isRequired,
  isEnabled: PropTypes.bool,
  loadingMessage: PropTypes.string
}

Spinner.defaultProps = {
  isEnabled: false,
  loadingMessage: LOADING_MESSAGE
}

export default Spinner
