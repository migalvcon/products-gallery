const styles = {
  loadingMessageBox: {
    fontSize: 18,
    padding: '30px 60px'
  },
  loadingMessageText: {
    display: 'inline-block',
    fontSize: 18
  },
  spinner: {
    display: 'inline-block',
    marginRight: 10,
    verticalAlign: 'middle'
  }
}

export default styles
