export const sleep = (ms) =>
  new Promise(resolve => setTimeout(resolve, ms))

export const byValue = (value, attribute) =>
  elem =>
    (value === '') || (value === undefined) ||
    (elem[attribute] && (elem[attribute].indexOf(value) !== -1))

export const byValueArray = (values, attribute) =>
  elem =>
    (values.length === 0) ||
    (elem[attribute] && (values.indexOf(elem[attribute]) !== -1))
