import { color, fontSize } from '../StyleConstants'

const styles = {
  error: {
    color: color.error,
    fontSize: fontSize.regular
  },
  icon: {
    marginRight: 8,
    verticalAlign: 'bottom',
    width: 20
  }
}

export default styles
