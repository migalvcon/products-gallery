import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Radium from 'radium'

import styles from './MessageStyles'

import errorIcon from './images/error-icon.png'

@Radium
class Message extends Component {
  render() {
    const {
      idPrefix,
      text
    } = this.props

    if (text === '') {
      return false
    }

    return (
      <div id={`${idPrefix}-message-container`}>
        <img src={errorIcon} style={styles.icon} alt="Error" />
        <span id={`${idPrefix}-message-text`} style={styles.error}>{text}</span>
      </div>
    )
  }
}

Message.propTypes = {
  idPrefix: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired
}

export default Message
