import { Component } from 'react'

class ResizeListenerComponent extends Component {
  constructor() {
    super()

    this.state = {
      windowWidth: window.innerWidth,
      mobileView: false
    }
  }

  componentDidMount() {
    window.addEventListener('resize',
      this.handleResize.bind(this))
  }

  componentWillUnmount() {
    window.removeEventListener('resize',
      this.handleResize.bind(this))
  }

  handleResize() {
    this.setState({ windowWidth: window.innerWidth })
  }

  handleMobileViewClick() {
    if(!this.state.mobileView) {
      this.setState({ mobileView: true })
    } else {
      this.setState({ mobileView: false })
    }
  }

  render() {
    return false
  }
}

export default ResizeListenerComponent
