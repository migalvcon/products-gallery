export const color = {
  default: '#000',
  fruit: '#ff8c00',
  grey: '#acacac',
  meat: '#dc143c',
  vegetable: '#2e8b57',
  blackTransparent: 'rgba(0, 0, 0, 0.45)',
  whiteTransparent: 'rgba(255, 255, 255, 0.75)',
  white: '#fff'
}

export const cursor = {
  selection: 'pointer'
}

export const fontFamily = {
  main: `Arial, 'Liberation Sans', FreeSans, sans-serif`
}

export const fontSize = {
  heading: 24,
  icon: 12,
  regular: 13
}
