import { cursor } from './StyleConstants'

const button = {
  borderRadius: 10
}

const styles = {
  inline: {
    display: 'inline-block'
  },
  button,
  clickable: {
    ...button,
    cursor: cursor.selection
  },
  block: {
    display: 'block'
  },
  wrapper: {
    marginLeft: 10,
    marginRight: 10
  }
}

export default styles
