import { color, fontFamily } from './common/StyleConstants'

const styles = {
  background: {
    backgroundColor: color.grey,
    color: color.default,
    fontFamily: fontFamily.main,
    minWidth: 280,
    minHeight: window.innerHeight,
    padding: 20
  }
}

export default styles
