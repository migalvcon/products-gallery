import { combineReducers } from 'redux'

import AppReducer from './AppReducer'
import FilterReducer from './filter/FilterReducer'
import GalleryReducer from './gallery/GalleryReducer'
import TopBarReducer from './topBar/TopBarReducer'

export default combineReducers({
  app: AppReducer,
  filter: FilterReducer,
  gallery: GalleryReducer,
  topBar: TopBarReducer
})
