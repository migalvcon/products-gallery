import {
  ID_PREFIX
} from '../AppConstants'

export const TOP_BAR_IDS = {
  BRANDNAME: `${ID_PREFIX}-top-bar-brandname`,
  MENU_ICON: `${ID_PREFIX}-top-bar-menu-icon`,
  MENU_ITEMS: `${ID_PREFIX}-top-bar-menu-items`,
  MENU_ITEMS_MOBILE: `${ID_PREFIX}-top-bar-menu-items-mobile`,
  WRAPPER: `${ID_PREFIX}-top-bar-wrapper`
}

export const SET_ITEM_SELECTED = 'SET_ITEM_SELECTED'
export const SET_MENU_ITEMS = 'SET_MENU_ITEMS'

export const MIN_WIDTH = 750

export const MENU_ICON_ALT_TEXT = 'Menu'
