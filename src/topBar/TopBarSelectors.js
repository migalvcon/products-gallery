export const itemSelectedSelector = (state) => state.topBar.itemSelected

export const menuItemsSelector = (state) => state.topBar.menuItems
