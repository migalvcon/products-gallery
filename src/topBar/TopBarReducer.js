import {
  SET_ITEM_SELECTED,
  SET_MENU_ITEMS
} from './TopBarConstants'

const initialState = {
  itemSelected: '',
  menuItems: []
}

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_ITEM_SELECTED: {
      return {
        ...state,
        itemSelected: action.itemSelected
      }
    }
    case SET_MENU_ITEMS: {
      return {
        ...state,
        menuItems: action.menuItems
      }
    }
    default: {
      return state
    }
  }
}
