import React from 'react'
import Radium from 'radium'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import {
  MENU_ICON_ALT_TEXT,
  MIN_WIDTH,
  TOP_BAR_IDS
} from './TopBarConstants'
import {
  BRANDNAME
} from '../AppConstants'

import {
  selectMenuItem
} from './TopBarActions'

import {
  itemSelectedSelector,
  menuItemsSelector
} from './TopBarSelectors'

import ResizeListenerComponent from '../common/ResizeListenerComponent'

import menuIcon from './images/menu-icon.png'

import styles from './TopBarStyles'

@Radium
export class TopBar extends ResizeListenerComponent {
  getItemStyle(itemId) {
    const {
      itemSelected
    } = this.props

    const itemStyle = itemSelected === itemId ? styles.itemSelected : styles.item

    return this.state.mobileView ? {...itemStyle, ...styles.block} : itemStyle
  }

  getMenuItemLinks() {
    const {
      menuItems,
      selectMenuItem
    } = this.props

    return menuItems.map(item => (
      <div key={item.id} style={this.getItemStyle(item.id)}
        onClick={() => selectMenuItem(item.id)}>{item.name}</div>
    ))
  }

  renderMobileView() {
    return this.state.mobileView ? this.getMenuItemLinks() : false
  }

  renderMenuItemLinks() {
    if(this.state.windowWidth <= MIN_WIDTH) {
      return(
        <div>
          <div id={TOP_BAR_IDS.MENU_ICON} onClick={this.handleMobileViewClick.bind(this)}>
            <img src={menuIcon} style={styles.menuIcon} alt={MENU_ICON_ALT_TEXT} />
          </div>
          <div id={TOP_BAR_IDS.MENU_ITEMS_MOBILE}>
            {this.renderMobileView()}
          </div>
        </div>
      )
    } else {
      return this.getMenuItemLinks()
    }
  }

  render() {
    return(
      <div id={TOP_BAR_IDS.WRAPPER} style={styles.wrapper}>
        <div id={TOP_BAR_IDS.BRANDNAME} style={styles.brandname}>{BRANDNAME}</div>

        <div id={TOP_BAR_IDS.MENU_ITEMS} style={styles.menuItems}>
          {this.renderMenuItemLinks()}
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    itemSelected: itemSelectedSelector(state),
    menuItems: menuItemsSelector(state)
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  selectMenuItem
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
) (TopBar)
