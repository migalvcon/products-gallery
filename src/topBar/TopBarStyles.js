import { color, fontSize } from '../common/StyleConstants'
import commonStyles from '../common/CommonStyles'

const {
  block,
  clickable,
  inline,
  wrapper
} = commonStyles

const item = {
  ...inline,
  fontSize: fontSize.regular,
  margin: 1,
  paddingBottom: 3,
  paddingLeft: 5,
  paddingRight: 5,
  paddingTop: 3,
  ':hover': {
    ...clickable,
    backgroundColor: color.white
  }
}

const styles = {
  block,
  brandname: {
    ...inline,
    fontSize: fontSize.heading,
    fontWeight: 'bold',
    verticalAlign: 'top',
    width: 150
  },
  item,
  itemSelected: {
    ...item,
    borderRadius: clickable.borderRadius,
    backgroundColor: color.white
  },
  menuIcon: {
    verticalAlign: 'middle',
    width: 30
  },
  menuItems: {
    ...inline,
    textAlign: 'right',
    width: 'calc(100% - 150px)'
  },
  wrapper: {
    ...wrapper,
    marginBottom: 20
  }
}

export default styles
