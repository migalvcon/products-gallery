import {
  SET_ITEM_SELECTED,
  SET_MENU_ITEMS
} from './TopBarConstants'

import {
  setError
} from '../AppActions'

import {
  getMenuItems
} from '../api/ProductsGalleryApi'

import {
  resetFilter
} from '../filter/FilterActions'

const setItemSelected = (itemSelected) => {
  return {
    type: SET_ITEM_SELECTED,
    itemSelected
  }
}

const setMenuItems = (menuItems) => {
  return {
    type: SET_MENU_ITEMS,
    menuItems
  }
}

export const loadMenuItems = (dispatch) =>
  getMenuItems()
  .then(menuItems => {
    dispatch(setMenuItems(menuItems))

    return Promise.resolve()
  })
  .catch(error => dispatch(setError(error)))

export const selectMenuItem = (itemSelected) => (dispatch) => {
  dispatch(setItemSelected(itemSelected))
  dispatch(resetFilter())
}
