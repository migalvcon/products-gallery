import React from 'react'
import { render } from 'react-dom'

import {
  initialize
} from './AppActions'

import configureStore from './store/configureStore'//eslint-disable-line import/default
import Root from './Root'
import RootReducer from './RootReducer'

const initState = RootReducer(undefined, { type: 'NOOP' })
const store = configureStore(initState)

render(<Root store={store} />, document.getElementById('root-container'))

initialize(store.dispatch)
