import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'

import RootReducer from '../RootReducer'

const finalCreateStore = compose(
  applyMiddleware(thunk),
  window.devToolsExtension ? window.devToolsExtension() : f => f
)(createStore)

export default function configureStore(initialState) {
  const store = finalCreateStore(RootReducer, initialState)

  if (module.hot) {
    module.hot.accept('../RootReducer', () =>
      store.replaceReducer(require('../RootReducer'))
    )
  }

  return store
}
