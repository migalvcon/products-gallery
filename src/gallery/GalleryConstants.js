import {
  ID_PREFIX
} from '../AppConstants'

export const GALLERY_IDS = {
  WRAPPER: `${ID_PREFIX}-gallery-wrapper`,
  PRODUCT: `${ID_PREFIX}-gallery-product`,
  PRODUCT_IMAGE: `${ID_PREFIX}-gallery-product-image`,
  PRODUCT_DETAIL: `${ID_PREFIX}-gallery-product-detail`,
  PRODUCT_HEADER: `${ID_PREFIX}-gallery-product-header`,
  PRODUCT_NAME: `${ID_PREFIX}-gallery-product-name`,
  PRODUCT_CATEGORY: `${ID_PREFIX}-gallery-product-category`,
  PRODUCT_DESCRIPTION: `${ID_PREFIX}-gallery-product-description`,
  PRODUCT_INFO: `${ID_PREFIX}-gallery-product-info`
}

export const SET_PRODUCTS = 'SET_PRODUCTS'

export const MORE_INFO = 'More info'
