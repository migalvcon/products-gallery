import {
  SET_PRODUCTS
} from './GalleryConstants'

import {
  setError
} from '../AppActions'

import {
  getProducts
} from '../api/ProductsGalleryApi'

const setProducts = (products) => {
  return {
    type: SET_PRODUCTS,
    products
  }
}

export const loadProducts = (dispatch) =>
  getProducts()
  .then(products => {
    dispatch(setProducts(products))

    return Promise.resolve()
  })
  .catch(error => dispatch(setError(error)))
