import React, { Component } from 'react'
import Radium from 'radium'
import { connect } from 'react-redux'

import {
  GALLERY_IDS
} from './GalleryConstants'

import {
  productsSelector
} from './GallerySelectors'
import {
  itemSelectedSelector
} from '../topBar/TopBarSelectors'
import {
  selectedFiltersSelector
} from '../filter/FilterSelectors'

import {
  byValue,
  byValueArray
} from '../common/CommonHelpers'

import Product from './Product'

@Radium
export class Gallery extends Component {
  render() {
    const {
      products
    } = this.props

    return(
      <div id={GALLERY_IDS.WRAPPER}>
        {
          products.map(product => (
            <Product key={product.id} product={product} />
          ))
        }
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const menuItem = itemSelectedSelector(state)
  const selectedFilters = selectedFiltersSelector(state)

  const filteredProducts = productsSelector(state)
    .filter(byValue(menuItem, 'menu'))
    .filter(byValueArray(selectedFilters, 'category'))

  return {
    products: filteredProducts
  }
}

export default connect(
  mapStateToProps
) (Gallery)
