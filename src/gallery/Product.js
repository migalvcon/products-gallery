import React, { Component } from 'react'
import Radium from 'radium'
import LinesEllipsis from 'react-lines-ellipsis'
import { connect } from 'react-redux'

import {
  GALLERY_IDS,
  MORE_INFO
} from './GalleryConstants'

import {
  filterListSelector
} from '../filter/FilterSelectors'

import {
  color
} from '../common/StyleConstants'
import styles from './GalleryStyles'

@Radium
export class Product extends Component {
  render() {
    const {
      filterList,
      product
    } = this.props

    const category = filterList.find(filter => filter.id === product.category)
    const categoryStyle = {
      ...styles.productCategory,
      backgroundColor: color[product.category]
    }

    return(
      <div id={GALLERY_IDS.PRODUCT} style={styles.product}>
        <div id={GALLERY_IDS.PRODUCT_IMAGE}>
          <img src={product.image} style={styles.productImage} />
        </div>
        <div id={GALLERY_IDS.PRODUCT_DETAIL} style={styles.productDetail}>
          <div id={GALLERY_IDS.PRODUCT_HEADER} style={styles.productHeader}>
            <div id={GALLERY_IDS.PRODUCT_NAME} style={styles.productName}>{product.name}</div>
            <div style={styles.productCategoryWrapper}>
              <div id={GALLERY_IDS.PRODUCT_CATEGORY} style={categoryStyle}>{category.name}</div>
            </div>
          </div>
          <div id={GALLERY_IDS.PRODUCT_DESCRIPTION} style={styles.productDescription}>
            <LinesEllipsis text={product.description} maxLine="8"
              ellipsis="..." trimRight basedOn="words" />
          </div>
          <div id={GALLERY_IDS.PRODUCT_INFO} style={styles.productInfo}>
            <span style={{color: color[product.category]}}>&gt;</span> {MORE_INFO}
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, ownProps) => {
  const {
    product
  } = ownProps

  return {
    filterList: filterListSelector(state),
    product: product
  }
}

export default connect(
  mapStateToProps
) (Product)
