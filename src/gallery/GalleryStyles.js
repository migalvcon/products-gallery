import { color, cursor, fontSize } from '../common/StyleConstants'
import commonStyles from '../common/CommonStyles'

const {
  inline,
  button
} = commonStyles

const styles = {
  product: {
    ...inline,
    margin: 10,
    verticalAlign: 'top',
    width: 256
  },
  productImage: {
    verticalAlign: 'middle'
  },
  productDetail: {
    backgroundColor: color.white,
    padding: 10
  },
  productHeader: {
    marginBottom: 20
  },
  productName: {
    ...inline,
    fontWeight: 'bold',
    verticalAlign: 'bottom',
    width: 'calc(100% - 80px)'
  },
  productCategory: {
    ...inline,
    ...button,
    color: color.white,
    fontSize: fontSize.icon,
    paddingBottom: 2,
    paddingLeft: 6,
    paddingRight: 6,
    paddingTop: 2
  },
  productCategoryWrapper: {
    ...inline,
    width: 80,
    textAlign: 'right'
  },
  productDescription: {
    fontSize: fontSize.regular,
    height: 120
  },
  productInfo: {
    fontSize: fontSize.regular,
    marginTop: 20,
    width: 75,
    ':hover': {
      cursor: cursor.selection,
      textDecoration: 'underline'
    }
  }
}

export default styles
