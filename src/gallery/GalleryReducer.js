import {
  SET_PRODUCTS
} from './GalleryConstants'

const initialState = {
  products: []
}

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_PRODUCTS: {
      return {
        ...state,
        products: action.products
      }
    }
    default: {
      return state
    }
  }
}
