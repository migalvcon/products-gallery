export const ID_PREFIX = 'products-gallery'

export const APP_IDS = {
  BACKGROUND: `${ID_PREFIX}-background-container`,
  MODAL: `${ID_PREFIX}-modal`,
  NOTIFICATION: `${ID_PREFIX}-notification`
}

export const SET_ERROR = 'SET_ERROR'
export const SET_READY = 'SET_READY'

export const BRANDNAME = 'Brandname'
export const MODAL_PLACEMENT = 'modal-placement'
