import {
  SET_FILTER_LIST,
  SET_SELECTED_FILTERS
} from './FilterConstants'

import {
  setError
} from '../AppActions'

import {
  getFilters
} from '../api/ProductsGalleryApi'

import {
  selectedFiltersSelector
} from './FilterSelectors'

const setFilterList = (filterList) => {
  return {
    type: SET_FILTER_LIST,
    filterList
  }
}

const setSelectedFilters = (selectedFilters) => {
  return {
    type: SET_SELECTED_FILTERS,
    selectedFilters
  }
}

export const loadFilters = (dispatch) =>
  getFilters()
  .then(filterList => {
    dispatch(setFilterList(filterList))

    return Promise.resolve()
  })
  .catch(error => dispatch(setError(error)))

export const addFilter = (selectedFilter) => (dispatch, getState) => {
  const selectedFilters = selectedFiltersSelector(getState())
  const pos = selectedFilters.indexOf(selectedFilter)

  if (pos === -1) {
    selectedFilters.push(selectedFilter)

    dispatch(setSelectedFilters(selectedFilters))
  }
}

export const removeFilter = (selectedFilter) => (dispatch, getState) => {
  const selectedFilters = selectedFiltersSelector(getState())
  const pos = selectedFilters.indexOf(selectedFilter)

  if (pos !== -1) {
    selectedFilters.splice(pos, 1)

    dispatch(setSelectedFilters(selectedFilters))
  }
}

export const resetFilter = () => (dispatch) =>
  dispatch(setSelectedFilters([]))
