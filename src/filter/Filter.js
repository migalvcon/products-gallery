import React from 'react'
import Radium from 'radium'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import {
  FILTER_IDS,
  MIN_WIDTH,
  TITLE
} from './FilterConstants'

import {
  addFilter,
  removeFilter
} from './FilterActions'

import {
  filterListSelector,
  selectedFiltersSelector
} from './FilterSelectors'

import CheckBox from '../form/checkBox/CheckBox'
import ResizeListenerComponent from '../common/ResizeListenerComponent'

import {
  color
} from '../common/StyleConstants'
import styles from './FilterStyles'

@Radium
export class Filter extends ResizeListenerComponent {
  containsFilter(filterId) {
    const {
      selectedFilters
    } = this.props

    return selectedFilters.indexOf(filterId) !== -1
  }

  performOperation(filterId) {
    const {
      addFilter,
      removeFilter
    } = this.props

    if (this.containsFilter(filterId)) {
      removeFilter(filterId)
    } else {
      addFilter(filterId)
    }

    this.forceUpdate()
  }

  getTitle() {
    return (this.state.windowWidth > MIN_WIDTH) ?
      (<div id={FILTER_IDS.TITLE} style={styles.title}>{TITLE}</div>) : false
  }

  render() {
    const {
      filterList
    } = this.props

    return(
      <div id={FILTER_IDS.WRAPPER} style={styles.wrapper}>
        {this.getTitle()}

        <div id={FILTER_IDS.FILTER_LIST} style={styles.filterList}>
          {
            filterList.map(filter => (
              <div key={filter.id} style={{...styles.filter, backgroundColor: color[filter.id]}}
                onClick={() => this.performOperation(filter.id)}>
                <CheckBox idPrefix={`${FILTER_IDS.FILTER_LIST}-${filter.id}`}
                  label={filter.name}
                  name={filter.id}
                  checked={this.containsFilter(filter.id)} />
              </div>
            ))
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    filterList: filterListSelector(state),
    selectedFilters: selectedFiltersSelector(state)
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  addFilter,
  removeFilter
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
) (Filter)
