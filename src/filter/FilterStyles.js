import { fontSize } from '../common/StyleConstants'
import commonStyles from '../common/CommonStyles'

const {
  inline,
  clickable,
  wrapper
} = commonStyles

const styles = {
  title: {
    ...inline,
    fontSize: fontSize.icon,
    marginRight: 5
  },
  filter: {
    ...inline,
    ...clickable,
    fontSize: fontSize.icon,
    marginLeft: 5,
    marginRight: 5,
    paddingLeft: 2,
    paddingRight: 8
  },
  filterList: {
    ...inline
  },
  wrapper: {
    ...wrapper,
    paddingLeft: 10
  }
}

export default styles
