import {
  ID_PREFIX
} from '../AppConstants'

export const FILTER_IDS = {
  FILTER_LIST: `${ID_PREFIX}-filter-list`,
  TITLE: `${ID_PREFIX}-filter-title`,
  WRAPPER: `${ID_PREFIX}-filter-wrapper`
}

export const TITLE = 'Filter:'

export const SET_FILTER_LIST = 'SET_FILTER_LIST'
export const SET_SELECTED_FILTERS = 'SET_SELECTED_FILTERS'

export const MIN_WIDTH = 350
