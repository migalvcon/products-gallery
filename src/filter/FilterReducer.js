import {
  SET_FILTER_LIST,
  SET_SELECTED_FILTERS
} from './FilterConstants'

const initialState = {
  filterList: [],
  selectedFilters: []
}

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_FILTER_LIST: {
      return {
        ...state,
        filterList: action.filterList
      }
    }
    case SET_SELECTED_FILTERS: {
      return {
        ...state,
        selectedFilters: action.selectedFilters
      }
    }
    default: {
      return state
    }
  }
}
