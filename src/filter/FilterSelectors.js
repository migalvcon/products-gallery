export const filterListSelector = (state) => state.filter.filterList

export const selectedFiltersSelector = (state) => state.filter.selectedFilters
