import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Radium from 'radium'

import styles from './CheckBoxStyles'

@Radium
class CheckBox extends Component {
  render() {
    const {
      idPrefix,
      label,
      name,
      checked
    } = this.props

    return (
      <div id={`${idPrefix}-checkbox`}>
        <input id={`${idPrefix}-checkbox-input`}
          name={name}
          type="checkbox"
          style={styles.box}
          checked={checked} />

        <label htmlFor={`${idPrefix}-checkbox-label`}
          style={styles.label}>{label}</label>
      </div>
    )
  }
}

CheckBox.propTypes = {
  idPrefix: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  checked: PropTypes.bool
}

CheckBox.defaultProps = {
  checked: false
}

export default CheckBox
