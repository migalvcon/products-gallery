import { color, cursor } from '../../common/StyleConstants'

const styles = {
  box: {
    cursor: cursor.selection
  },
  label: {
    color: color.white,
    cursor: cursor.selection
  }
}

export default styles
