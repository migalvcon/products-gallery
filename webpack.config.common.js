'use strict'

const path = require('path')

module.exports = {
  resolve: {
    extensions: [ '.js', '.jsx' ]
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: [ 'babel-loader' ],
        include: path.join(__dirname, 'src'),
        exclude: [ /joi-browser/ ]
      },
      {
        test: /\.(png|gif)$/,
        use: [{
          loader: 'url-loader',
          options: {
            limit: 100000
          }
        }]
      },
      {
        test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
        use: [ 'file-loader' ]
      }
    ]
  }
}
