## Requirements

You will need `npm` and `node` in order to build the `dist` file. The commands you have to execute are the next:

```bash
npm install
npm run build
```

It will generate a bundle in the `dist` folder.

## Run locally

Open the `index.html` file in a browser once you generate the `dist` file. **However, there is a dist file ready for using**.

## Hot Module Replacement (HMR)

HMR is the ability to replace JS modules while the project is running without reloading the page. The next command allows you to modify the application and see the changes immediately:

```bash
npm run start
```

After that, open in a browser the url http://localhost:3000.

## Tests

If you want to test the application, then execute the next command:

```bash
npm test
```

## Coverage

The application contains an option that allows you to see the code coverage. Execute the next command:

```bash
npm run coverage
```

And open the file `coverage/lcov-report/index.html` in a browser.
