// Prevent Mocha from compiling class
function loader(module, filename) {
  return module._compile('module.exports = \'' + filename.replace(__dirname, '') + '\'', filename)
}

require.extensions['.css'] = loader
require.extensions['.json'] = loader
require.extensions['.txt'] = loader
require.extensions['.gif'] = loader
require.extensions['.jpg'] = loader
require.extensions['.png'] = loader
