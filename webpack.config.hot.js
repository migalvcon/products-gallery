'use strict'

const path = require('path')
const webpack = require('webpack')
const objectAssign = require('object-assign')
const commonConfig = require('./webpack.config.common')

const config = objectAssign(commonConfig, {
  devtool: 'cheap-module-eval-source-map',
  context: __dirname,
  entry: [
    'babel-polyfill', // necessary for Array.find Object.keys in IE
    'eventsource-polyfill', // necessary for hot reloading with IE
    'webpack-hot-middleware/client'
  ],
  output: {
    path: __dirname,
    filename: 'react-bundle.js',
    publicPath: '/dist/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),

    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('development')
      }
    })
  ]
})

module.exports = function (mode) {
  const entry = mode || './src/index.js'

  config.entry.push(entry)

  return config
}
